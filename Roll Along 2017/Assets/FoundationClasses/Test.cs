﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using LibraryOfSnazziness;

public class Test : MonoBehaviour {

	public Text textTxt;

	void Start () {
		InvokeRepeating ("CountDown", 0f, 1f);
	}

	void CountDown ()  {
		string text = fnd_CountDown.CountDownWeeklyStringFull (4, 12, 35, 00);
		textTxt.text = text.ToUpper();
	}

}