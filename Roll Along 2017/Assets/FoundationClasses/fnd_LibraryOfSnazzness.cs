﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LibraryOfSnazziness {
	
	public class fnd_ShopFunctions : MonoBehaviour {
		///<summary>
		///Set your coins varibale equal to this to make a purchase and adjust the price. The item's name will be saved in PlayerPrefs as "item_" + itemName and you can use this to determine which items are owned.
		///</summary>
		public static int BuyItem (string itemName, int price, int coins)
		{
			switch (CheckItemIsPurchasable(itemName, price, coins)) {

			case true:
				coins = coins - price;
				PlayerPrefs.SetInt ("item_" + itemName + "_Owned", 1);
				Debug.Log ("Purchase Made");
				return coins;
				break;
			default:
				Debug.LogError ("Error Making Purchase");
				return coins;
				break;
			}
		}

		static bool CheckItemIsPurchasable (string itemName, int price, int coins)
		{
			if (PlayerPrefs.GetInt ("_item" + itemName) == 0) {
				if (price > coins) {
					return false;
				} else {
					return true;
				}
			} else {
				Debug.Log ("Item already owned");
				return false;
			}
		}
	}

	public class fnd_Random : MonoBehaviour {

		///<summary>
		///This function will return a random gameObject from a list that you specifiy.
		///</summary>
		public static GameObject RandomGameObjectFromList (List<GameObject> gameObjectList)
		{
			int randomNumber = Random.Range (0, gameObjectList.Count);
			GameObject chosenGameObject = gameObjectList [randomNumber];
			return chosenGameObject;
		}

		///<summary>
		///This function will return a random gameObject several lists that you specificy. You must also specificy how likely each list is to be chosen with an integer. The higher the integer, the more likely it is to be selected.
		///</summary>
		public static GameObject WeightedRandomGameObjectFromList (List<GameObject> commonGameObjectsList, int commonChance, List<GameObject> rareGameObjectsList, int rareChance, List<GameObject> legendaryGameObjectsList, int legendaryChance)
		{
			int randomListChoice = Random.Range (0, commonChance + rareChance + legendaryChance);
			GameObject chosenGameObject = null;

			if (randomListChoice < legendaryChance) {
				int randomNumber = Random.Range (0, legendaryGameObjectsList.Count);
				chosenGameObject = legendaryGameObjectsList [randomNumber];
			} else if (randomListChoice < legendaryChance + rareChance) {
				int randomNumber = Random.Range (0, rareGameObjectsList.Count);
				chosenGameObject = rareGameObjectsList [randomNumber];
			} else {
				int randomNumber = Random.Range (0, commonGameObjectsList.Count);
				chosenGameObject = commonGameObjectsList [randomNumber];
			}

			return chosenGameObject;

		}
	}

	public class fnd_CountDown : MonoBehaviour
	{
		///<summary>
		/// This function will return the number of seconds until a specified day of the week and time.
		/// The integer daysOfWeek can be between 0 and 6 with 0 representing Monday and 6 representing Sunday. The hour integer runs on a 24 hour clock. E.G. 13 would be 1pm.
		///</summary>
		public static int SecondsUntilCertainDayEveryWeek (int dayOfWeek, int Hour, int Minute, int Second)
		{

			int secondsUntil = Second - System.DateTime.Now.Second;
			int minutesUntil = Minute - System.DateTime.Now.Minute;
			int hoursUntil = Hour - System.DateTime.Now.Hour;

			int secondsUntilTimeMatches = secondsUntil + (minutesUntil * 60) + (hoursUntil * 60 * 60);

			int dayToGetTo = 0;

			switch (System.DateTime.Now.DayOfWeek.ToString ()) {

			case "Monday":
				dayToGetTo = 0;
				break;
			case "Tuesday":
				dayToGetTo = 1;
				break;
			case "Wednesday":
				dayToGetTo = 2;
				break;
			case "Thursday":
				dayToGetTo = 3;
				break;
			case "Friday":
				dayToGetTo = 4;
				break;
			case "Saturday":
				dayToGetTo = 5;
				break;
			case "Sunday":
				dayToGetTo = 6;
				break;
			default:
				dayToGetTo = dayOfWeek;
				break;
			}
			int daysUntilDesiredDay = dayOfWeek - dayToGetTo;
			if (daysUntilDesiredDay < 0) {
				daysUntilDesiredDay = 7 + daysUntilDesiredDay;
			}
			if (daysUntilDesiredDay == 0 && secondsUntilTimeMatches < 0) {
				daysUntilDesiredDay = 7 + daysUntilDesiredDay;
			}

			int secondsUntilDesiredDay = daysUntilDesiredDay * 24 * 60 * 60;

			int timeUntilSpecifiedDateAndTime = secondsUntilDesiredDay + secondsUntilTimeMatches;

			return timeUntilSpecifiedDateAndTime;

		}

		///<summary>
		/// This function will return the number of seconds until a specified day of the week and time.
		/// The integer daysOfWeek can be between 0 and 6 with 0 representing Monday and 6 representing Sunday. The hour integer runs on a 24 hour clock. E.G. 13 would be 1pm.
		///</summary>
		static string CountDownWeeklyString (int dayOfWeek, int Hour, int Minute, int Second, bool full)
		{

			int daysUntilChosenDay = LibraryOfSnazziness.fnd_CountDown.SecondsUntilCertainDayEveryWeek (dayOfWeek,Hour,Minute,Second);

			int daysUntil = 0;
			int hoursUntil = 0;
			int minutesUntil = 0;
			int secondsUntil = 0;

			if (daysUntilChosenDay > (60 * 60 * 24)) {
				daysUntil = Mathf.FloorToInt (daysUntilChosenDay / (60 * 60 * 24));
			}
			if (daysUntilChosenDay - (daysUntil * 60 * 60 * 24) > (60 * 60)) {
				hoursUntil = Mathf.FloorToInt ((daysUntilChosenDay - (daysUntil * 60 * 60 * 24)) / (60 * 60));
			}
			if (daysUntilChosenDay - (daysUntil * 60 * 60 * 24) - (hoursUntil * 60 * 60) > 60) {
				minutesUntil = Mathf.FloorToInt ((daysUntilChosenDay - (daysUntil * 60 * 60 * 24) - (hoursUntil * 60 * 60)) / (60));
			}
			if (daysUntilChosenDay - (daysUntil * 60 * 60 * 24) - (hoursUntil * 60 * 60) - (minutesUntil * 60) > 0) {
				secondsUntil = Mathf.FloorToInt ((daysUntilChosenDay - (daysUntil * 60 * 60 * 24) - (hoursUntil * 60 * 60) - (minutesUntil * 60)));
			}

			string targetDay;

			switch (dayOfWeek) {

			case 0:
				targetDay = "Monday";
				break;
			case 1:
				targetDay = "Tuesday";
				break;
			case 2:
				targetDay = "Wednesday";
				break;
			case 3:
				targetDay = "Thursday";
				break;
			case 4:
				targetDay = "Friday";
				break;
			case 5:
				targetDay = "Saturday";
				break;
			case 6:
				targetDay = "Sunday";
				break;
			default:
				targetDay = "N/A";
				break;
			}

			string targetHour;
			string PMorAM;

			if (Hour <= 12) {
				targetHour = Hour.ToString ();
				PMorAM = "AM";
			} else {
				targetHour = (Hour - 12).ToString ();
				PMorAM = "PM";
			}

			string daysString = " Days ";
			if (daysUntil == 1) {
				daysString = " Day ";
			}

			string hoursString = " Hours ";
			if (hoursUntil == 1) {
				hoursString = " Hour ";
			}

			string minutesString = " Minutes ";
			if (minutesUntil == 1) {
				minutesString = " Minute ";
			}

			string secondsString = " Seconds ";
			if (secondsUntil == 1) {
				secondsString = " Second ";
			}

			string returnString;

			if (full == true) {
				returnString = daysUntil + daysString + hoursUntil + hoursString + minutesUntil + minutesString + secondsUntil + secondsString + "until: " + targetDay + ", " + targetHour + ":" + Minute.ToString ("00") + ":" + Second.ToString ("00") + " " + PMorAM;
			} else {
				if (daysUntil != 0) {
					returnString = daysUntil + daysString + "until: " + targetDay + ", " + targetHour + ":" + Minute.ToString ("00") + ":" + Second.ToString ("00") + " " + PMorAM;
				} else {
					returnString = hoursUntil + hoursString + minutesUntil + minutesString + secondsUntil + secondsString + "until: " + targetDay + ", " + targetHour + ":" + Minute.ToString ("00") + ":" + Second.ToString ("00") + " " + PMorAM;

				}
			}

			return returnString;

		}

		public static string CountDownWeeklyStringFull (int dayOfWeek, int Hour, int Minute, int Second)
		{
			string returnString = CountDownWeeklyString (dayOfWeek, Hour, Minute, Second, true);
			return returnString;
		}

		public static string CountDownWeeklyStringPart (int dayOfWeek, int Hour, int Minute, int Second)
		{
			string returnString = CountDownWeeklyString (dayOfWeek, Hour, Minute, Second, false);
			return returnString;
		}

	}

}
